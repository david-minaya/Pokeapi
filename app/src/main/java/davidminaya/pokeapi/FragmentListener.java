package davidminaya.pokeapi;

import java.util.ArrayList;

/**
 * Creada por david minaya el 09/02/2018 11:38.
 */

public interface FragmentListener {
    void lanzarPokemonViewPager(int posicion, ArrayList<Pokemon> pokemones);
    void cerrarPokemonViewPager();
}
