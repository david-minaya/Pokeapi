package davidminaya.pokeapi;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david minaya on 17/07/2017.
 * 1:19
 */

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.VistaHolder>{

    public interface InterfazRecicleView{
        void onItemClickListener(View view, int position);
    }

    public static InterfazRecicleView interfazRecicleView;

    private List<Pokemon> pokemones = new ArrayList<>();

    public PokemonAdapter(InterfazRecicleView interfazRecicleView){
        this.interfazRecicleView = interfazRecicleView;
    }

    @Override
    public VistaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon_adapter, parent, false);

        VistaHolder vistaHolder = new VistaHolder(vista);

        return vistaHolder;
    }

    public static class VistaHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        TextView nombre;
        ImageView imageView;

        public VistaHolder(View itemView) {
            super(itemView);

            imageView = (ImageView)itemView.findViewById(R.id.imagen);
            nombre = (TextView) itemView.findViewById(R.id.texto);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            interfazRecicleView.onItemClickListener(v, getAdapterPosition());
        }
    }

    @Override
    public void onBindViewHolder(VistaHolder holder, int position) {

        holder.nombre.setText(pokemones.get(position).getNombre());
        holder.imageView.setImageBitmap(pokemones.get(position).getImagen());
    }

    @Override
    public int getItemCount() {
        return pokemones.size();
    }

    public void actualizar(Pokemon pokemon) {

        try {
            pokemones.remove(pokemon);
            pokemones.add(pokemon);
        } catch (Exception e) {
            pokemones.add(pokemon);
        }

        notifyDataSetChanged();
    }
}