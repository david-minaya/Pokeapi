package davidminaya.pokeapi;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david minaya on 03/02/2018 3:57.
 */

public class Pokemon implements Parcelable{

    public static final String CLAVE = "pokemon";

    private String nombre;
    private Bitmap imagen;
    private String descripcion;
    private String tipo;
    private List<Estadistica> estadisticas;

    {
        estadisticas = new ArrayList<>();
    }

    public Pokemon() {

    }

    public Pokemon(String nombre) {
        this.nombre = nombre;
    }

    public Pokemon(String nombre, Bitmap imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public Pokemon(String nombre, Bitmap imagen, String descripcion, String tipo, List<Estadistica> estadisticas) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.estadisticas = estadisticas;
    }

    protected Pokemon(Parcel in) {
        nombre = in.readString();
        imagen = in.readParcelable(Bitmap.class.getClassLoader());
        descripcion = in.readString();
        tipo = in.readString();
        in.readTypedList(estadisticas, Estadistica.CREATOR);
    }

    public static final Creator<Pokemon> CREATOR = new Creator<Pokemon>() {
        @Override
        public Pokemon createFromParcel(Parcel in) {
            return new Pokemon(in);
        }

        @Override
        public Pokemon[] newArray(int size) {
            return new Pokemon[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() { return descripcion; }

    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Estadistica> getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(List<Estadistica> estadisticas) { this.estadisticas = estadisticas; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeParcelable(imagen, flags);
        dest.writeString(descripcion);
        dest.writeString(tipo);
        dest.writeTypedList(estadisticas);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pokemon)) return false;

        Pokemon pokemon = (Pokemon) o;

        if (getNombre() != null ? !getNombre().equals(pokemon.getNombre()) : pokemon.getNombre() != null)
            return false;
        return getImagen() != null ? getImagen().equals(pokemon.getImagen()) : pokemon.getImagen() == null;
    }

    @Override
    public int hashCode() {
        int result = getNombre() != null ? getNombre().hashCode() : 0;
        result = 31 * result + (getImagen() != null ? getImagen().hashCode() : 0);
        return result;
    }

    public static class Estadistica implements Parcelable {

        private int valor;
        private Enum constantes;

        public Estadistica() {}

        public Estadistica(int valor, Enum constantes) {
            this.valor = valor;
            this.constantes = constantes;
        }

        protected Estadistica(Parcel in) {
            valor = in.readInt();
            constantes = in.readParcelable(Enum.class.getClassLoader());
        }

        public static final Creator<Estadistica> CREATOR = new Creator<Estadistica>() {
            @Override
            public Estadistica createFromParcel(Parcel in) {
                return new Estadistica(in);
            }

            @Override
            public Estadistica[] newArray(int size) {
                return new Estadistica[size];
            }
        };

        public int getValor() {
            return valor;
        }

        public void setValor(int valor) {
            this.valor = valor;
        }

        public Enum getConstantes() {
            return constantes;
        }

        public void setConstantes(Enum constantes) {
            this.constantes = constantes;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeFloat(valor);
            dest.writeParcelable(constantes, flags);
        }
    }

    public enum Enum implements Parcelable{

        SALUD("Salud", "hp", 1f, Color.parseColor("#03e203")),
        ATAQUE("Ataque", "attack", 1.416f, Color.parseColor("#ff0059")),
        DEFENSA("Defensa", "defense", 1.108f, Color.parseColor("#0088ff")),
        VELOCIDAD("Velocidad", "speed", 1.416f, Color.parseColor("#ffea00"));

        private String nombre;
        private String clave;
        private float escala;
        private @ColorInt int color;

        Enum(String nombre, String clave, float escala, @ColorInt int color) {
            this.nombre = nombre;
            this.clave = clave;
            this.escala = escala;
            this.color = color;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(nombre);
            dest.writeString(clave);
            dest.writeFloat(escala);
            dest.writeInt(color);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Enum> CREATOR = new Creator<Enum>() {
            @Override
            public Enum createFromParcel(Parcel in) {
                return Enum.values()[in.readInt()];
            }

            @Override
            public Enum[] newArray(int size) {
                return new Enum[size];
            }
        };

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getClave() {
            return clave;
        }

        public float getEscala() {
            return escala;
        }

        public void setEscala(float escala) {
            this.escala = escala;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }

    }

}
