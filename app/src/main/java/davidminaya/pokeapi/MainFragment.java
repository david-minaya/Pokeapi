package davidminaya.pokeapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static davidminaya.pokeapi.Pokemon.Enum.ATAQUE;
import static davidminaya.pokeapi.Pokemon.Enum.DEFENSA;
import static davidminaya.pokeapi.Pokemon.Enum.SALUD;
import static davidminaya.pokeapi.Pokemon.Enum.VELOCIDAD;

/**
 * Creada por david minaya el 07/02/2018 2:06.
 */

public class MainFragment extends Fragment implements OnClickListener {

    private String tag = "MainFragment";

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ImageView imgError;
    private Button reintentar;

    private RequestQueue queue;
    private PokemonAdapter pokemonAdapter;
    private ArrayList<Pokemon> pokemones = new ArrayList<>();
    private Pokemon pokemon;

    private FragmentListener fragmentListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        imgError = (ImageView)view.findViewById(R.id.img_error);
        reintentar = (Button)view.findViewById(R.id.reintentar);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        pokemonAdapter = new PokemonAdapter(new PokemonAdapter.InterfazRecicleView() {
            @Override
            public void onItemClickListener(View view, int position) { fragmentListener.lanzarPokemonViewPager(position, pokemones); }
        });
        recyclerView.setAdapter(pokemonAdapter);
        imgError.setVisibility(GONE);
        reintentar.setVisibility(GONE);
        reintentar.setOnClickListener(this);

        queue = Volley.newRequestQueue(getActivity());
        getListPokemon();

        return view;
    }

    int j = 0;
    private void getListPokemon(){

        Log.i(tag, "getListPokemon() -> Realizando peticion https://pokeapi.co/api/v2/pokemon/");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "https://pokeapi.co/api/v2/pokemon/", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(tag, "getListPokemon() -> Reponse.Listener() exitosa");
                        try {
                            JSONArray results = response.getJSONArray("results");

                            if (j < results.length())
                                getPokemon(results, j);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, "getListPokemon() -> Reponse.ErrorListener() "+error.toString());
                        progressBar.setVisibility(GONE);
                        imgError.setVisibility(VISIBLE);
                        reintentar.setVisibility(VISIBLE);
                    }
                }
        );
        request.setTag(tag);
        queue.add(request);
    }

    private void getPokemon(final JSONArray reponseh, final int i) throws JSONException {

        final String nombre = reponseh.getJSONObject(i).getString("name");
        String urlPokemon = reponseh.getJSONObject(i).getString("url");

        Log.i(tag, i+" getPokemon() -> Realizando peticion "+nombre);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlPokemon, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(tag, i+" getPokemon() -> Reponse.Listener() "+nombre+" exitosa");
                        try {
                            pokemon = new Pokemon();
                            getNombreDescripcionYTipo(response, pokemon, nombre, i);
                            getEstadisticas(response, pokemon);
                            getImagen(response, pokemon, nombre, i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        j++;
                        try {
                            getPokemon(reponseh, j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, "getPokemon() -> Reponse.ErrorListener() "+nombre+" "+error.toString());
                        j++;
                        try {
                            getPokemon(reponseh, j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        request.setTag(tag);
        queue.add(request);
    }

    private void getEstadisticas(JSONObject reponse, Pokemon pokemon) throws JSONException {

        JSONArray stats = reponse.getJSONArray("stats");

        List<Pokemon.Estadistica> estadisticas = new ArrayList<>();
        for (int i = 0; i < stats.length(); i++) {

            String nombre = stats.getJSONObject(i).getJSONObject("stat").getString("name");

            if (nombre.equals(VELOCIDAD.getClave())) {
                int valor = stats.getJSONObject(i).getInt("base_stat");
                estadisticas.add(new Pokemon.Estadistica(valor, VELOCIDAD));

            } else if (nombre.equals(DEFENSA.getClave())) {
                int valor = stats.getJSONObject(i).getInt("base_stat");
                estadisticas.add(new Pokemon.Estadistica(valor, DEFENSA));

            } else if (nombre.equals(ATAQUE.getClave())) {
                int valor = stats.getJSONObject(i).getInt("base_stat");
                estadisticas.add(new Pokemon.Estadistica(valor, ATAQUE));

            } else if (nombre.equals(SALUD.getClave())) {
                int valor = stats.getJSONObject(i).getInt("base_stat");
                estadisticas.add(new Pokemon.Estadistica(valor, SALUD));
            }
        }

        pokemon.setEstadisticas(estadisticas);
    }

    private void getNombreDescripcionYTipo(final JSONObject response, final Pokemon pokemon, final String nombre, final int i) throws JSONException {

        String urlSpecies = response.getJSONObject("species").getString("url");

        Log.i(tag, i+" getNombreDescripcionYTipo -> Realizando peticion "+nombre);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlSpecies, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(tag, i+" getNombreDescripcionYTipo -> Reponse.Listener() "+nombre+" exitosa");
                        try {

                            String nombre = "";
                            JSONArray names = response.getJSONArray("names");
                            for (int i = 0; i < names.length(); i++) {
                                String idioma = names.getJSONObject(i).getJSONObject("language").getString("name");
                                if (idioma.equals("es")) {
                                    nombre = names.getJSONObject(i).getString("name");
                                    break;
                                }
                            }
                            pokemon.setNombre(nombre);

                            String descripcion = "";
                            JSONArray flavorTextEntries = response.getJSONArray("flavor_text_entries");
                            for (int i = 0; i < flavorTextEntries.length(); i++) {
                                String idioma = flavorTextEntries.getJSONObject(i).getJSONObject("language").getString("name");
                                if (idioma.equals("es")) {
                                    descripcion = flavorTextEntries.getJSONObject(i).getString("flavor_text").replace("\n", " ");
                                    break;
                                }
                            }
                            pokemon.setDescripcion(descripcion);

                            String tipo = "";
                            JSONArray tipos = response.getJSONArray("genera");
                            for (int i = 0; i < tipos.length(); i++) {
                                String idioma = tipos.getJSONObject(i).getJSONObject("language").getString("name");
                                if (idioma.equals("es")) {
                                    tipo = tipos.getJSONObject(i).getString("genus");
                                    break;
                                }
                            }
                            pokemon.setTipo(tipo);
                            actualizarPokemon(pokemon);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, i+" getNombreDescripcionYTipo -> Reponse.ErrorListener() "+nombre+" "+error.toString());
                    }
                }
        );
        request.setTag(tag);
        queue.add(request);

    }

    private void getImagen(JSONObject response, final Pokemon pokemon, final String nombre, final int i) throws JSONException {

        JSONObject sprites = response.getJSONObject("sprites");
        String urlfront_default = sprites.getString("front_default");

        Log.i(tag, i+" getImagen() -> Realizando peticion "+nombre);
        ImageRequest request = new ImageRequest(urlfront_default,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        Log.i(tag, i+" getImagen() -> Reponse.Listener() "+nombre+" exitosa");
                        pokemon.setImagen(response);
                        actualizarPokemon(pokemon);
                    }
                },
                0, 0, null, null,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, i+" getImagen() -> Reponse.ErrorListener() "+nombre+" "+error.toString());
                    }
                }
        );
        request.setTag(tag);
        queue.add(request);

    }

    private void actualizarPokemon(Pokemon pokemon) {

        progressBar.setVisibility(GONE);

        pokemonAdapter.actualizar(pokemon);
        try {
            pokemones.remove(pokemon);
            pokemones.add(pokemon);
        } catch (Exception e) {
            pokemones.add(pokemon);
        }
    }

    @Override
    public void onClick(View view) {
        imgError.setVisibility(GONE);
        reintentar.setVisibility(GONE);
        progressBar.setVisibility(VISIBLE);
        getListPokemon();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentListener = (FragmentListener) context;
    }
}
