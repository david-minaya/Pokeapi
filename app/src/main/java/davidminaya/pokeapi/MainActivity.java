package davidminaya.pokeapi;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FragmentListener{

    private static final String tag = "MainActivity";

    InfoFragment infoFragment;
    PokemonViewPager pokemonViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_fragment, new MainFragment())
                .commit()
        ;

        infoFragment = new InfoFragment();
        pokemonViewPager = new PokemonViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        try {

            item.setEnabled(false);

            final View view = infoFragment.getView();

            int right = view.getRight();
            int top = view.getTop();
            int width = view.getWidth();
            int height = view.getHeight();
            float radio = (float) Math.sqrt(width * width + height * height);

            Animator anim = ViewAnimationUtils.createCircularReveal(view, right, top, radio, 0);
            anim.setDuration(900);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    item.setEnabled(true);
                    infoFragment.getActivity().onBackPressed();
                }
            });
            anim.start();


            item.setIcon(R.drawable.ic_info_outline_white_24dp);
        } catch (NullPointerException e) {
            item.setEnabled(true);
            Log.i(tag, "Abrir InfoFragment");
            item.setIcon(R.drawable.ic_cancel_white_24dp);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.add(R.id.main_fragment, infoFragment);
            transaction.commit();
        }

        return true;
    }

    @Override
    public void lanzarPokemonViewPager(int posicion, ArrayList<Pokemon> pokemones) {
        pokemonViewPager = PokemonViewPager.newInstance(posicion, pokemones);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.zoom_out, R.anim.zoom_in, R.anim.zoom_out, R.anim.zoom_in);
        transaction.add(R.id.main_fragment, pokemonViewPager);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void cerrarPokemonViewPager() {
        pokemonViewPager.getActivity().onBackPressed();
    }

}
