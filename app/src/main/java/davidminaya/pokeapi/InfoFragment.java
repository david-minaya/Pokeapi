package davidminaya.pokeapi;

import android.animation.Animator;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

/**
 * Creada por david minaya el 07/02/2018 12:26.
 */

public class InfoFragment extends Fragment implements OnClickListener {

    private TextView pokeapi, gitLab, email;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.info_fragment, container, false);

        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop,
                                       int oldRight, int oldBottom) {

                int radio = (int) Math.hypot(right, bottom);

                Animator reveal = ViewAnimationUtils.createCircularReveal(v, right, top, 0, radio);
                reveal.setInterpolator(new DecelerateInterpolator(2f));
                reveal.setDuration(1000);
                reveal.start();
            }
        });

        pokeapi = (TextView)view.findViewById(R.id.pokeapi);
        gitLab = (TextView)view.findViewById(R.id.gitlab);
        email = (TextView)view.findViewById(R.id.email);

        pokeapi.setOnClickListener(this);
        gitLab.setOnClickListener(this);
        email.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        TextView textView = (TextView) view;

        if (textView.getId() == R.id.pokeapi)
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://pokeapi.co")));

        else if (textView.getId() == R.id.gitlab)
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/david-minaya/Pokeapi")));

        else if (textView.getId() == R.id.email)
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:davidminaya04@gmail.com")));
    }



}
