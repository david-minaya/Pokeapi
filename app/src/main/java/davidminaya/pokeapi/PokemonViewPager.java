package davidminaya.pokeapi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david minaya on 11/08/2017.
 * 7:58
 */

public class PokemonViewPager extends Fragment {

    public static final String POSICION = "posicion";

    private int posicion;
    private ArrayList<Pokemon> pokemones;
    ViewPager viewPager;
    AdaptadorDetalle adaptadorDetalle;

    public static PokemonViewPager newInstance(int posicion, ArrayList<Pokemon> pokemones) {
        Bundle bundle = new Bundle();
        bundle.putInt(POSICION, posicion);
        bundle.putParcelableArrayList(Pokemon.CLAVE, pokemones);
        PokemonViewPager pokemonViewPager = new PokemonViewPager();
        pokemonViewPager.setArguments(bundle);
        return pokemonViewPager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        posicion = getArguments().getInt(POSICION);
        pokemones = getArguments().getParcelableArrayList(Pokemon.CLAVE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.detalle_fragment, container, false);

        List<PokemonFragment> detallesFragments = new ArrayList<>();
        for (Pokemon pokemon : pokemones) {
            detallesFragments.add(PokemonFragment.newInstance(pokemon));
        }

        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        adaptadorDetalle = new AdaptadorDetalle(getFragmentManager(), detallesFragments);
        viewPager.setAdapter(adaptadorDetalle);
        viewPager.setCurrentItem(posicion);

        return rootView;
    }

    public class AdaptadorDetalle extends FragmentStatePagerAdapter {

        private List<PokemonFragment> pokemonFragments;

        public AdaptadorDetalle(FragmentManager fm, List<PokemonFragment> pokemonFragments) {
            super(fm);
            this.pokemonFragments = pokemonFragments;
        }

        @Override
        public Fragment getItem(int position) {
            return pokemonFragments.get(position);
        }

        @Override
        public int getCount() {
            return pokemonFragments.size();
        }

    }

}
