package davidminaya.pokeapi;

import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Creada por david minaya el 04/02/2018 4:49.
 */

public class EstadisticasAdapter extends RecyclerView.Adapter<EstadisticasAdapter.VistaHolder> {

    List<Pokemon.Estadistica> estadisticas;
    WindowManager windowManager;
    OnClickListener onClickListener;

    public EstadisticasAdapter(List<Pokemon.Estadistica> estadisticas, WindowManager windowManager, OnClickListener onClickListener) {
        this.estadisticas = estadisticas;
        this.windowManager = windowManager;
        this.onClickListener = onClickListener;
    }

    @Override
    public EstadisticasAdapter.VistaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.estadisticas_adapter, parent, false);

        VistaHolder vistaHolder = new VistaHolder(view, onClickListener);

        return vistaHolder;
    }

    public static class VistaHolder extends RecyclerView.ViewHolder implements OnClickListener{

        private TextView nombre;
        private ImageView barra;
        private OnClickListener onClickListener;

        public VistaHolder(View itemView, OnClickListener onClickListener) {
            super(itemView);
            this.onClickListener = onClickListener;
            nombre = (TextView) itemView.findViewById(R.id.nombre);
            barra = (ImageView) itemView.findViewById(R.id.barra);

            itemView.setOnClickListener(this);
        }

        public TextView getNombre() {
            return nombre;
        }

        public ImageView getBarra() {
            return barra;
        }

        @Override
        public void onClick(View v) {
            onClickListener.onClick(v);
        }
    }

    @Override
    public void onBindViewHolder(VistaHolder holder, int position) {

        int valor = estadisticas.get(position).getValor();
        String titulo = estadisticas.get(position).getConstantes().getNombre() +"  "+ valor;
        int color = estadisticas.get(position).getConstantes().getColor();
        float escala = estadisticas.get(position).getConstantes().getEscala();

        int escalar = (int) (valor * escala);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int densidad = (int) displayMetrics.density;
        int dp = escalar * densidad;

        holder.getNombre().setText(titulo);
        holder.getBarra().setMinimumWidth(dp);
        holder.getBarra().setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return estadisticas.size();
    }
}
