package davidminaya.pokeapi;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Creada por david minaya el 04/02/2018 11:56.
 */

public class PokemonFragment extends Fragment implements OnClickListener{

    private ImageView imagen;
    private TextView nombre;
    private TextView descripcion;
    private TextView tipo;
    private RecyclerView estadisticas;
    private EstadisticasAdapter estadisticasAdapter;
    private LinearLayout linearLayout;

    private Pokemon pokemon;

    FragmentListener fragmentListener;

    public static PokemonFragment newInstance(Pokemon pokemon) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Pokemon.CLAVE, pokemon);
        PokemonFragment pokemonFragment = new PokemonFragment();
        pokemonFragment.setArguments(bundle);
        return pokemonFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pokemon = getArguments().getParcelable(Pokemon.CLAVE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.pokemon_fragment, container, false);

        linearLayout = (LinearLayout) rootView.findViewById(R.id.linear_layout);
        imagen = (ImageView)rootView.findViewById(R.id.imagen);
        nombre = (TextView)rootView.findViewById(R.id.nombre);
        descripcion = (TextView)rootView.findViewById(R.id.descripcion);
        tipo = (TextView)rootView.findViewById(R.id.tipo);
        estadisticas = (RecyclerView)rootView.findViewById(R.id.estadisticas);

        nombre.setText(pokemon.getNombre());
        imagen.setImageBitmap(pokemon.getImagen());
        descripcion.setText(pokemon.getDescripcion());
        tipo.setText(pokemon.getTipo());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        estadisticas.setLayoutManager(layoutManager);
        estadisticasAdapter = new EstadisticasAdapter(pokemon.getEstadisticas(), getActivity().getWindowManager(), this);
        estadisticas.setAdapter(estadisticasAdapter);
        linearLayout.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        fragmentListener.cerrarPokemonViewPager();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentListener = (FragmentListener) context;
    }
}
